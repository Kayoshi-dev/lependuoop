﻿using System;
using System.Collections.Generic;

namespace LePendu
{
    public class PenduGame : AbstractGame
    {
        private string _wordToFind;
        private List<char> _wrongChar = new List<char>();
        
        public PenduGame()
        {
            _wordToFind = Initialize();

            Console.WriteLine(_wordToFind);
            NewRound();
        }

        private void NewRound()
        {
            if (NumberError != 10)
            {
                Console.Write("Type a letter to find the word: ");
                char c = PickAChar();
                ComputeAnswer(c);
                Console.WriteLine(DiscWordToFind);

                if (_wrongChar.Count != 0)
                {
                    Console.WriteLine("Mauvais charactères : ");
                    foreach (var wrong in _wrongChar)
                    {
                        Console.WriteLine(wrong + ".");
                    }    
                }

                string discWordToFindString = new string(DiscWordToFind);
                int compareOrdinal = String.CompareOrdinal(_wordToFind, discWordToFindString);
                if (compareOrdinal == 0)
                {
                    Console.WriteLine("Vous avez gagné!");
                }
                else
                {
                    NewRound();
                }
            }
            else
            {
                Console.WriteLine("Vous avez perdu!");
            }
        }
        
        private char PickAChar()
        {
            string t = Console.ReadLine();
            
            while (String.IsNullOrEmpty(t) || String.IsNullOrWhiteSpace(t))
            {
                t = Console.ReadLine();
            }
            
            return t.ToUpper().ToCharArray()[0];
        }

        private void ComputeAnswer(char c)
        {
            List<int> occurencesChar = _wordToFind.AllIndexesOf(c.ToString());
            if (occurencesChar.Count != 0)
            {
                foreach (var correctLetter in occurencesChar)
                {
                    DiscWordToFind[correctLetter] = c;
                }
                
                Console.WriteLine("Bonne lettre!");
            }
            else
            {
                Console.WriteLine("Mauvaise lettre!");
                _wrongChar.Add(c);
                NumberError++;
            }
        }
    }
}