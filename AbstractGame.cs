﻿using System;
using System.IO;
using System.Text;

namespace LePendu
{
    public abstract class AbstractGame
    {
        protected int NumberError { get; set; }
        protected char[] DiscWordToFind { get; set; }
        
        protected string Initialize()
        {
            string[] lines = File.ReadAllLines(@"words.txt", Encoding.UTF8);
            Random random = new Random();
            int lineNumber = random.Next(1, lines.Length);
            NumberError = 0;
            string linePure = lines[lineNumber].Trim();
            DiscWordToFind = new char[linePure.Length];
            for (int i = 0; i < linePure.Length; i++)
            {
                DiscWordToFind[i] = '_';
            }

            return linePure;
        }
    }
}